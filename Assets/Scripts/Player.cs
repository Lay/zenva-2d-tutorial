﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public float moveSpeed;
    public Rigidbody2D rig;
    public float jumpForce;
    public SpriteRenderer sr;
    public int score;
    public UI ui;

    // FixedUpdate is called every 0.02 second - used mainly for working with physics
    void FixedUpdate()
    {
        float xInput = Input.GetAxis("Horizontal");
        rig.velocity = new Vector2(xInput * moveSpeed, rig.velocity.y);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow) && isGrounded())
        {
            rig.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);   
        }

        if (rig.velocity.x > 0)
        {
            sr.flipX = false;
        }
        else if (rig.velocity.x < 0)
        {
            sr.flipX = true;
        }
    }

    bool isGrounded()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position + new Vector3(0, -0.1f, 0), Vector2.down, 0.2f);
        return hit.collider != null;
    }

    public void GameOver()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void AddScore(int amount)
    {
        score += amount;
        ui.SetScoreText(score);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Anchor1"))
        {
            if (rig.velocity.x < 0)
            {
                ui.SetPrimaryText("There's nothing this way");
            }
            else
            {
                ui.SetPrimaryText("zzzZZZZzzZZZzzz");
            }
        }
        else if (collision.gameObject.CompareTag("Anchor2"))
        {
            if (rig.velocity.x < 0)
            {
                ui.SetPrimaryText("Seriously don't waste your time");
            }
            else
            {
                ui.SetPrimaryText("...");
            }
        }
        else if (collision.gameObject.CompareTag("Anchor3"))
        {
            if (rig.velocity.x < 0)
            {
                ui.SetPrimaryText("OK you win. Hurray...");
            }
            else
            {
                ui.SetPrimaryText("Now enjoy walking all the way back");
            }           
        }
    }
}
